#pragma once
#include "gameObject.h"
#include "inventory.h"
#include "component.h"
#include <string>

class iCanHazInventory:component
{
public:
	iCanHazInventory();
	virtual gameObject* locate(std::string id)= 0;
	~iCanHazInventory();
};

