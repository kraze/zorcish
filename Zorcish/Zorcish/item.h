#pragma once
#include "gameObject.h"
#include "pickupableComponent.h"

using namespace std;

class item :
	public gameObject
{
private:
	bool pickupable;
	bool isInv;
	pickupableComponent* pickComp;
public:
	item();
	item(string identifier, string aName, string aDescription, bool aPickup, bool aInv);
	item(string identifier, string aName, string aDescription, pickupableComponent* aPickup, bool aInv);
	bool getPickupable();
	~item();
};

